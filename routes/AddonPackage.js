const express = require('express');
const router = express.Router();
const AddonPackageController = require('../app/api/controllers/AddonPackage');
router.get('/get',AddonPackageController.getAddonPackage);
module.exports = router;