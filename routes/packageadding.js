const express = require('express');
const router = express.Router();
const packageAddController = require('../app/api/controllers/packageadding');
router.post('/add',packageAddController.addPackage);
router.get('/get',packageAddController.GetPackageAndWashingHistory);
router.post('/washing',packageAddController.Wasing);
module.exports = router;