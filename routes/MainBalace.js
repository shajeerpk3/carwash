const express = require('express');
const router = express.Router();
const MainBalanceController = require('../app/api/controllers/MainBalace');
router.post('/add',MainBalanceController.addOrDeductBalance);
router.get('/get',MainBalanceController.getPaydetails);
module.exports = router;