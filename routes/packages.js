const express = require('express');
const router = express.Router();
const packagesController = require('../app/api/controllers/packages');
router.post('/addpackage', packagesController.addpackage);
router.get('/getpackage', packagesController.getpackage);
module.exports = router;
