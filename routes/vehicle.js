const express = require('express');
const router = express.Router();
const vehicleController = require('../app/api/controllers/vehicle');
router.post('/add',vehicleController.addvehicle);
router.get('/find',vehicleController.findVehicle);
module.exports = router;

