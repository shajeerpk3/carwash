const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const packageaddingSchema = new Schema(
    {
        packageId:String,
        packageName: String,
        vehicleNumber:String,
        Adddate: Date,
        status:Boolean,
        Amount:Number,
        wasingHistory:[{
            serviceDate:Date,
            lastCount:String,
            freeCountStatus:Boolean,
            AddOnPackage:[{
                AddOnAmount:String,
                AddOnId:String,
                AddOnName:String,
            }]
        }],
        limit:Number,
        complete:Number,
        free:Number
    }
);

module.exports = mongoose.model('PackageAdding', packageaddingSchema);