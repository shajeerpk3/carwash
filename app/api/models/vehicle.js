const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const vehicleSchema = new Schema(
    {
        userid:String,
        model: String,
        number:{
            type: String,
            trim: true,
            required: true
         },
        maker: String,
        status:Boolean,
        vehicletype:String,
        value:String,
        label:String
    }
);

module.exports = mongoose.model('vehicle', vehicleSchema);