const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;
// const packagesSchema = new Schema({
//  package: {
//   type: String
//  },
//  type: {
//   type: String,
//   trim: true,
//   required: true
//  },
//  rate: {
//   type: Number
//  },
//  limit: {
//     type: Number
//    },
//    free: {
//     type: Number
//     }
// });


const packagesSchema = new Schema(
  {
    name: String,
    type: String,
    rate: Number,
    limit: Number,
    free: Number,
  }
);

// hash user password before saving into database
// packagesSchema.pre('save', function(next){
// this.password = bcrypt.hashSync(this.password, saltRounds);
// next();
// });
module.exports = mongoose.model('Packages', packagesSchema);