const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const AddonPackageSchema = new Schema(
    {
        _id:String,
        updatedAt: String,
        createdAt:String,
        description: String,
        normalrate:Number,
        rate:Number
    }
);

module.exports = mongoose.model('AddonPackage', AddonPackageSchema);