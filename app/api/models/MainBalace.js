const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MainBalanceSchema = new Schema(
    {
        UserId:String,
        MainBalance: Number,
        BalanceHistory:[{
            Date:Date,
            Amount:Number,
            AmountAction:String,//Add or deduct
            Action:String,//Package/Reacharge/addOn
            PayRef:String,
            paydetails:{
                BillName:String,
                Address:String,
                AddressLin1:String,
                Amount:String,
                Post:String,
                ContactNum:String,
                paymentToken:String,
                orderID:String,
                payerID:String
            },
            PayMode:String,
            VehicleNO:String,
            AddPackageId:String,
            PackageName:String,
            AddOnPackage:[{
                AddOnAmount:Number,
                AddOnId:String,
                AddOnName:String,
            }]
        }],
      
    }
);

module.exports = mongoose.model('Payment', MainBalanceSchema);