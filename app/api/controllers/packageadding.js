const packageaddingModel = require('../models/packageadding.js')
const MainBalanceModel = require('../models/MainBalace.js')

module.exports = {
    
    Wasing: function (req, res, next) {
        


        packageaddingModel.findOne({ vehicleNumber: req.query.vehicleNumber, status: true }, function (err, packageaddingInfo) {

          
            
            var AddOnPackage=[]
           
            if (err) {
                next(err);
            }
            else if (!packageaddingInfo) {
                res.json({ status: "error", message: "No Package found for this vehicle", data: null });
            } else {
                //Set current washing free of washing status.
                var freeStatus = false
                if (+packageaddingInfo.complete + 1 > packageaddingInfo.limit) {
                    freeStatus = true
                }
              
                var wasingHistorys =
                {
                    serviceDate: Date.now(),
                    lastCount: packageaddingInfo.complete,
                    freeCountStatus: freeStatus,
                    AddOnPackage: AddOnPackage
                }


                //Deactivate Package from vehicle if is count complete
                if ((+packageaddingInfo.complete + 1) == (+packageaddingInfo.limit + +packageaddingInfo.free)) {
                    packageaddingInfo.status = false
                }

                //increase count
                packageaddingInfo.complete = +packageaddingInfo.complete + 1
             
                //update washing history and addon details
                console.log(packageaddingInfo.wasingHistory.length);
                if(packageaddingInfo.wasingHistory.length==0){
                    packageaddingInfo.wasingHistory=wasingHistorys;
                }
                else{
                packageaddingInfo.wasingHistory.push(wasingHistorys);
                }
              

                
                //deduct from balance if available addon
                if (AddOnPackage.length != 0) {

                    MainBalanceModel.findOne({ UserId: req.body.userId }, function (err, MainBalanceInfo) {//check main balance for activated
                     
                        if (MainBalanceInfo == null) {
                            res.json({ status: "error", message: "Insufficient Balance, Please add main balance", data: null });

                            // next(err);
                        }

                        else {


                            if (MainBalanceInfo.MainBalance < req.query.Amount) {//check current main balance amount
                                res.json({ status: "error", message: "Insufficient Balance, Please add main balance", data: null });
                                // next(err);
                            }
                            else {




                                //Calculate Main Balance info
                                var Balance =
                                {
                                    Date: Date.now(),
                                    Amount: req.query.Amount,
                                    AmountAction: "deduct",
                                    Action: "addOn",
                                    PayRef: null,
                                    paydetails: {},
                                    PayMode: null,
                                    VehicleNO: packageaddingInfo.vehicleNumber,
                                    AddPackageId: packageaddingInfo._id,
                                    PackageName: packageaddingInfo.packageName,
                                    AddOnPackage: AddOnPackage
                                }

                                MainBalanceInfo.BalanceHistory.push(Balance);
                                if (req.query.AmountAction == "Add") { MainBalanceInfo.MainBalance = +MainBalanceInfo.MainBalance + +req.query.Amount }
                                else { { MainBalanceInfo.MainBalance = +MainBalanceInfo.MainBalance - +req.query.Amount } }
                              
                                MainBalanceInfo.save();
                                packageaddingInfo.save();
                                res.json({ status: "success", message: "Washing Updated", data: null });





                            }
                        }

                    });

                }
                else{
                    packageaddingInfo.save();
                    res.json({ status: "success", message: "Washing Updated", data: null });
                }


               

            }
        });
    },


    addPackage: function (req, res, next) {



        packageaddingModel.findOne({ vehicleNumber: req.query.vehicleNumber, status: true }, function (err, vehicleInfo) {//check if package activated for this vehicle
          
            if (vehicleInfo != null) {
                res.json({ status: "error", message: "already one package active for this vehicle", data: null });
                // next(err);
            }

            else {
                MainBalanceModel.findOne({ UserId: req.body.userId }, function (err, MainBalanceInfo) {//check main balance for activated

                    if (MainBalanceInfo == null) {
                        res.json({ status: "error", message: "Insufficient Balance, Please add main balance", data: null });
                        // next(err);
                    }

                    else {


                        if (MainBalanceInfo.MainBalance < req.query.Amount) {//check current main balance amount
                            res.json({ status: "error", message: "Insufficient Balance, Please add main balance", data: null });
                            // next(err);
                        }
                        else {

                            packageaddingModel.create({ packageId: req.body.packageId, packageName: req.query.packageName, vehicleNumber: req.query.vehicleNumber, Adddate: Date.now(), status: true, limit: req.query.limit, complete: req.query.complete, free: req.query.free, PackageAmount: req.query.Amount }, function (err, result) {//add package
                                if (err) {
                                    res.json({ status: "error", message: "package  added fail", data: null });
                                } else {


                                    //Calculate Main Balance info
                                    var Balance =
                                    {
                                        Date: Date.now(),
                                        Amount: req.query.Amount,
                                        AmountAction: req.query.AmountAction,
                                        Action: req.query.Action,
                                        PayRef: req.query.PayRef,
                                        paydetails: {},
                                        PayMode: req.query.PayMode,
                                        VehicleNO: req.query.VehicleNO,
                                        AddPackageId: result._id,
                                        PackageName: req.query.PackageName
                                    }
                                    console.log("balanceee",Balance)
                                    MainBalanceInfo.BalanceHistory.push(Balance);
                                    if (req.query.AmountAction == "Add") { MainBalanceInfo.MainBalance = +MainBalanceInfo.MainBalance + +req.query.Amount }
                                    else { { MainBalanceInfo.MainBalance = +MainBalanceInfo.MainBalance - +req.query.Amount } }

                                    MainBalanceInfo.save();
                                    res.json({ status: "success", message: "package added", data: null });
                                }

                            });


                        }
                    }

                });
            }
        });
    },

   

    GetPackageAndWashingHistory: function (req, res, next) {

        packageaddingModel.find({ vehicleNumber: req.query.vehicleNumber, status: true }, function (err, packageaddingInfo) {
            console.log("dateee3333",Date.now())
            if (err) {
                next(err);
            } else {
                res.json({ status: "success", message: "Package Adding Details", data: { packageaddingInfo: packageaddingInfo } });
            }
        });
    },

   


}