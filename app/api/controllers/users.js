var userModel = require('../models/users.js');
var vehicleModel = require('../models/vehicle.js')

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = {
   create: function (req, res, next) {
      userModel.findOne({ email: req.query.email }, function (err, userInfo) {
         if (userInfo != null) {
            res.json({ status: "error", message: "Email Id already exist", data: null });
            // next(err);
         }
         else if (err) {
            res.json({ status: "error", message: "Register fail1", data: null });
         }
         else {

            userModel.create({ name: req.query.name, email: req.query.email, password: req.query.password, mobileno: req.query.mobileno }, function (err, result) {
               if (err)
                  next(err);
               else {
                  if (req.query.number != null) {
                     userModel.findOne({ email: req.query.email }, function (err, userInfo) {
                        if (userInfo == null || err) {
                           res.json({ status: "error", message: "User added Vehile info adding fail 2", data: null });
                           // next(err);
                        }
                        else {
                           vehicleModel.create({ userid: userInfo._id, model: req.query.model, number: req.query.number, maker: req.query.maker ,status: false,vehicletype:null,value:req.query.number,label:req.query.number}, function (err, result) {
                              if (err) {
                                 next(err);
                              } else {
                                 res.json({ status: "success", message: "user and vehicle details added", data: userInfo });
                              }

                           });
                        }


                     })
                  }

               }
            });
         }
      });


   },
   authenticate: function (req, res, next) {
      // console.log("WELCOME");

      //  console.log("res",  res);
      userModel.findOne({ email: req.query.email }, function (err, userInfo) {
         console.log("BODY-WELCOME", err, userInfo);
         if (userInfo == null || err) {
            res.json({ status: "error", message: "Invalid email/password !The user name is Case Sensitive", data: null });
            // next(err);
         } else {
            if (bcrypt.compareSync(req.query.password, userInfo.password)) {
               const token = jwt.sign({ id: userInfo._id }, req.app.get('secretKey'), { expiresIn: '1h' });
               res.json({ status: "success", message: "user found!!!", data: { user: userInfo, token: token } });
            } else {
               res.json({ status: "error", message: "Invalid email/password", data: null });
            }
         }
      });
   },
}