
const vehicleModel = require('../models/vehicle.js')


module.exports = {
    addvehicle: function (req, res, next) {
        console.log(" req.body.userId", req.body.userId)
        vehicleModel.findOne({ number: req.query.number }, function (err, vehicleInfo) {
            if (vehicleInfo != null) {
                res.json({ status: "error", message: "Vehicle number already exist", data: null });
                // next(err);
            }
            else if (err) {
                res.json({ status: "error", message: "Vehicle added fail1", data: null });
            }
            else {
                vehicleModel.create({ userid: req.body.userId, model: req.query.model, number: req.query.number, maker: req.query.maker, status: false ,vehicletype:null,value:req.query.number,label:req.query.number}, function (err, result) {
                    if (err) {
                        res.json({ status: "error", message: "vehicle details added fail2", data: null });
                    } else {
                        res.json({ status: "success", message: "vehicle details added", data: null });
                    }

                });
            }
        });
    },

    findVehicle: function (req, res, next) {
        console.log(" req.body.userId", req.body.userId)
        vehicleModel.find({ userid: req.body.userId }, function (err, vehicleInfo) {
            
            if (err) {
                res.json({ status: "error", message: "Vehicle find fail1", data: null });
            }
            else{
                res.json({ status: "success", message: "Vehicles found", data: vehicleInfo });
            }
        });
    }
}