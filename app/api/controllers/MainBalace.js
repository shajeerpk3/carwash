const MainBalanceModel = require('../models/MainBalace')


module.exports = {
    addOrDeductBalance: function (req, res, next) {
        console.log(" req.body.userId", req.body.userId)
        var paydetails = {
            BillName: req.query.Name,
            Address: req.query.Address,
            AddressLin1: req.query.AddressLin1,
            Amount: req.query.Amount,
            Post: req.query.Post,
            ContactNum: req.query.ContactNum,
            paymentToken: req.query.PayRef,
            orderID: req.query.orderID,
            payerID: req.query.payerID
        }

        var Balance =
        {
            Date: Date.now(),
            Amount: req.query.Amount,
            AmountAction: req.query.AmountAction,
            Action: req.query.Action,
            PayRef: req.query.PayRef,
            paydetails: paydetails,
            PayMode: req.query.PayMode,
            VehicleNO: req.query.VehicleNO,
            AddPackageId: req.query.AddPackageId,
            PackageName: req.query.PackageName,
            AddOnPackage: req.query.AddOnPackage
        }

        MainBalanceModel.findOne({ UserId: req.body.userId }, function (err, BalanceInfo) {
            console.log("vehicleinfo", BalanceInfo)
            if (BalanceInfo == null) {
                MainBalanceModel.create({ UserId: req.body.userId, MainBalance: req.query.Amount, BalanceHistory: Balance }, function (err, result) {
                    if (err) {
                        res.json({ status: "error", message: "Payment added fail", data: null });
                    } else {
                        res.json({ status: "success", message: "Payment added", data: null });
                    }

                });
            }

            else {
                BalanceInfo.BalanceHistory.push(Balance);
                if (req.query.AmountAction == "Add") { BalanceInfo.MainBalance = +BalanceInfo.MainBalance + +req.query.Amount }
                else { { BalanceInfo.MainBalance = +BalanceInfo.MainBalance - +req.query.Amount } }

                BalanceInfo.save();
                res.json({ status: "success", message: "Payment added", data: null });
            }
        });
    },

    getPaydetails: function (req, res, next) {

        MainBalanceModel.find({UserId: req.body.userId}, function (err, BalanceInfo) {
            if (err) {
                next(err);
            } else {
                res.json({ status: "success", message: "Payment Details", data: { BalanceInfo: BalanceInfo } });
            }
        });
    },

}