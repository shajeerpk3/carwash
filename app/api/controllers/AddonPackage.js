const AddonPackageModel = require('../models/AddonPackage');

module.exports = {
  
 
  getAddonPackage: function (req, res, next) {

    AddonPackageModel.find({}, function (err, packages) {
      if (err) {
        next(err);
      } else {
        res.json({ status: "success", message: "Add On package", data: { AddonPackage: packages } });
      }
    });
  },
}